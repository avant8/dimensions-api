<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mechanic extends Model
{
    protected $fillable = [
        'name', 'action', 'str', 'hp', 'dex', 'int', 'cha', 'notes', 'user_id', 'image', 'traits'
    ];
    public function type()
    {
        return $this->hasOne('App\Mechanic\Type');
    }

    public function nodes()
    {
        return $this->belongsToMany('App\Mechanic');
    }

    public function decisions()
    {
        return $this->belongsToMany('App\Decision');
    }

    public function execute($lua)
    {
        if ($this->script)
            $action = file_get_contents(storage_path($this->script));
        else
            $action = $this->action;

        $variables = $lua->eval($action);
        return $variables;
    }
}
