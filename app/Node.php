<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Node extends Model
{
    protected $fillable = [
        'name', 'story_id', 'node_type_id', 'description'
    ];
    use Traits\HasMechanics;

    public function type()
    {
        return $this->hasOne('App\Node\Type');
    }

    public function parent()
    {
        return $this->hasOne('App\Node', 'id', 'parent_id');
    }


    public function monsters()
    {
        return $this->belongsToMany('App\Monster');
    }
    public function items()
    {
        return $this->belongsToMany('App\Item');
    }

    public function decisions()
    {
        return $this->belongsToMany('App\Decision');
    }

    public function processStory()
    {
        $story = "Isla Invictus leans in and looks at your group, then whispers as though the very walls are trying to listen in. <monster name=test> Are you sure that you're ready for what you'll find inside? Often that which is sought is not that which is required";
        $pat_attributes = "(\S+)=(\"|'| |)(.*)(\"|'| |>)";

        preg_match_all("@$pat_attributes@isU", $story, $ms);
        var_dump($ms);

        die();
        return $story;
    }
}
