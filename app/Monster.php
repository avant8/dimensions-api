<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Monster extends Model
{
    use Traits\HasMechanics;

    public $parent, $target, $node;
    protected $fillable = [
        'name', 'description', 'str', 'hp', 'dex', 'int', 'cha', 'notes', 'user_id', 'image', 'traits'
    ];

    public function nodes()
    {
        return $this->belongsToMany('App\Node', 'monster_node', 'monster_id', 'node_id');
    }
}
