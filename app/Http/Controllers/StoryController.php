<?php

namespace App\Http\Controllers;

use App\Story;
use App\Decision;
use App\Node;
use Illuminate\Http\Request;
use App\Http\Resources\StoryCollection;
use App\Http\Resources\Story as StoryResource;
use Rap2hpoutre\FastExcel\FastExcel;
use App\Monster;
use App\Item;
use Illuminate\Support\Facades\Log;
use App\Mechanic;

class StoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new StoryCollection(Story::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function import(Request $request)
    {
        $title = "The Haunting of Rosedrift";
        $nodes = array();
        $decisions = [];
        $monsters = array();

        Story::where('name', '=', $title)->delete();

        $story = Story::create(['name' => $title, 'description' => " "]);
        $storyId = $story->id;
        $lastNode = false;

        $import = new FastExcel;

        $objects = $import->import(storage_path('app/story/Haunted.xlsx'));

        foreach ($objects as $line) {
            //Assign variables from array into variables, for easier access
            ['Type' => $type, 'Name' => $name, 'Description' => $description, 'Parents' => $parent,  'Target' => $target,  'Condition' => $condition, 'Label' => $label] = $line;
            ['HP' => $hp, 'STR' => $str, 'DEX' => $dex, 'CHA' => $cha, 'INT' => $int] = $line;
            ['Required STR' => $reqStr, 'Required CHA' => $reqCha, 'Required INT' => $reqInt, 'Required DEX' => $reqDex, 'Notes' => $notes] = $line;

            $target = $target ? $target : $name;
            $label = $label ? $label : $name;

            switch ($type) {
                case "Node":
                    $node = Node::create([
                        'name' => $name,
                        'story_id' => $storyId,
                        'node_type_id' => 1,
                        'description' => $description
                    ]);

                    $nodes[$name] = $node;

                    if ($parent) {
                        $parents = explode(PHP_EOL, $parent);

                        //foreach ($parents as $parent) {
                        if (array_key_exists($parents[0], $nodes) && !$node->parent_id)
                            $node->parent_id = $nodes[$parents[0]]->id;

                        $node->save();

                        $decision = Decision::create([
                            'destination_node_id' => $node->id,
                            'name'  => $label
                        ]);

                        $decision->target = $name;
                        $decision->node = $parent;
                        $decisions[] = $decision;
                        //}
                    }
                    $lastNode = $node;

                    break;
                case "Decision":
                    $parent = $parent ? $parent : $lastNode->name;
                    //parents = explode(PHP_EOL, $parent);

                    //foreach ($parents as $parent) {
                    $decision = Decision::create([
                        'name'      => $label
                    ]);

                    $decision->target = $target;
                    $decision->node = $parent;
                    $decisions[] = $decision;
                    //}

                    break;
                case "Monster":
                    $parent = $parent ? $parent : $lastNode->name;
                    //$parents = explode(PHP_EOL, $parent);

                    //foreach ($parents as $parent) {
                    $monster = Monster::create([
                        'name'          => $name,
                        'description'   => $description,
                        'hp'            => $hp,
                        'str'           => $str,
                        'int'           => $int,
                        'cha'           => $cha,
                        'dex'           => $dex,
                        'notes'         => $notes
                    ]);

                    Log::debug("STR:" . $monster->str);
                    $monster->node = $parent;
                    $monsters[] = $monster;
                    //}

                    break;
                case "Item":
                    $item = Item::create([
                        'name'          => $name,
                        'description'   => $description,
                        'hp'            => $hp,
                        'str'           => $str,
                        'int'           => $int,
                        'cha'           => $cha,
                        'dex'           => $dex,
                        'notes'         => $notes
                    ]);

                    $items[$target][] = $item;
                    break;
                case "Mechanic":
                    $parent = $parent ? $parent : $lastNode->name;
                    $code = "";
                    switch ($name) {
                        case "SetVariable":
                            $code = "return {" . $target . "=" . $condition . "}";
                            break;
                        case "PrependStory":
                            if ($target) {
                                $code = "if " . $target . $condition . " then" . PHP_EOL;
                            }
                            $code .= "prependStory('" . $description . "')" . PHP_EOL;
                            if ($target) {
                                $code = "end" . PHP_EOL;
                            }
                            //$code = "return {" . $target . "=" . $condition . "}";
                            break;
                        case "AppendStory":
                            if ($target) {
                                $code = "if " . $target . $condition . " then" . PHP_EOL;
                            }
                            $code .= "appendStory('" . $description . "')" . PHP_EOL;
                            if ($target) {
                                $code = "end" . PHP_EOL;
                            }
                            //$code = "return {" . $target . "=" . $condition . "}";
                            break;
                        default:
                            $code = $description;
                            break;
                    }
                    $mechanic = Mechanic::create([
                        'name'          => $name,
                        'action'        => $code,
                    ]);
                    $mechanic->node = $parent;
                    $mechanics[] = $mechanic;
                    break;
            }
        }

        foreach ($decisions as $decision) {
            if (array_key_exists($decision->target, $nodes)) {
                $decision->destination_node_id = $nodes[$decision->target]->id;
                $decision->save();

                Log::debug("Adding Decision '" . $decision->name . " to Node '" . $decision->node . "'. Destination Node ID: " . $decision->destination_node_id);
                $parents = explode(PHP_EOL, $decision->node);
                foreach ($parents as $parent) {
                    $nodes[$parent]->decisions()->attach($decision);
                }
            } else {
                Log::warning("Couldn't add Decision. Node " . $decision->target . " not found in array.");
            }
        }

        foreach ($monsters as $monster) {
            if (array_key_exists($monster->node, $nodes)) {
                Log::debug("Adding Monster '" . $monster->name . " to Node '" . $monster->node);

                $parents = explode(PHP_EOL, $monster->node);
                foreach ($parents as $parent) {
                    $nodes[$parent]->monsters()->attach($monster);
                }
            } else {
                Log::warning("Couldn't add Monster. Node " . $monster->node . " not found in array.");
            }
        }

        foreach ($mechanics as $mechanic) {
            if (array_key_exists($mechanic->node, $nodes)) {
                Log::debug("Adding Mechanic '" . $mechanic->id . " to Node '" . $mechanic->node);

                $parents = explode(PHP_EOL, $mechanic->node);
                foreach ($parents as $parent) {
                    $nodes[$parent]->mechanics()->attach($mechanic);
                }
            } else {
                Log::warning("Couldn't add Mechanic. Node " . $mechanic->node . " not found in array.");
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Story  $story
     * @return \Illuminate\Http\Response
     */
    public function show(Story $story)
    {
        return response()->json($story, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Story  $story
     * @return \Illuminate\Http\Response
     */
    public function edit(Story $story)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Story  $story
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Story $story)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Story  $story
     * @return \Illuminate\Http\Response
     */
    public function destroy(Story $story)
    {
        //
    }
}
