<?php

namespace App\Http\Controllers;

use App\Game;
use App\Node;
use App\Decision;
use App\Character;
use App;

use Illuminate\Http\Request;
use App\Http\Resources\Game as GameResource;

class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //TODO: Set node_id to root node of story
        $game =  Game::create([
            'story_id' => $request->get('story_id'),
            'user_id' => $request->get('user_id')
        ]);

        $firstNode = Node::where('story_id', $request->get('story_id'))
            ->whereNull('parent_id')
            ->first();

        if ($firstNode) {
            $game->node_id = $firstNode->id;
            $game->save();
        }

        return new GameResource($game);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function decision(Game $game, $destinationId, $decisionId = null)
    {
        $game->node_id = $destinationId;
        $game->save();

        if ($decisionId) {
            $decision = Decision::find($decisionId);
            foreach ($decision->mechanics as $mechanic)
                $game->triggerMechanic($mechanic);
        }

        return $this->show($game);;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function show(Game $game)
    {
        $game->load('story', 'node', 'characters');

        $node = \App\Node::find($game->node_id)->load('decisions', 'mechanics', 'monsters', 'items');

        foreach ($node->mechanics as $mechanic)
            $game->triggerMechanic($mechanic);

        $template = view('node', ['node' => $node])->render();

        //Combine monsters from mechanics with node monsters
        $monsters = $node->monsters;
        if (count($monsters) > 0) {
            if (is_array(session("monsters")))
                $monsters = array_merge($node->monsters->toArray(), session("monsters"));
        } else {
            $monsters = session("monsters");
        }

        //Combine items from mechanics with node monsters
        $items = $node->items;
        if (count($items) > 0) {
            if (is_array(session("items")))
                $items = array_merge($node->items->toArray(), session("items"));
        } else {
            $items = session("items");
        }

        //Combine items from mechanics with node monsters
        $decisions = $node->decisions;
        if (count($decisions) > 0) {
            if (is_array(session("decisions")))
                $decisions = array_merge($node->decisions->toArray(), session("decisions"));
        } else {
            $decisions = session("decisions");
        }

        $resource = new GameResource($game);
        $resource->additional(['data' => ['mechanics' => $node->mechanics, 'monsters' => $monsters, 'characters' => $game->characters, 'decisions' => $decisions,  'items' => $items, 'template' => $template]]);


        return $resource;
    }

    public function resetGame($node)
    {
        session(["monsters" => "", "items" => "", "story" => "",]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function edit(Game $game)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Game $game)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function destroy(Game $game)
    {
        //
    }
}
