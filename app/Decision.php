<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Decision extends Model
{
    protected $table = 'decisions';
    public $timestamps = false;
    public $parent, $target, $node;

    protected $fillable = [
        'name', 'variable', 'node_id', 'destination_node_id'
    ];

    public function destination_node()
    {
        return $this->belongsTo('App\Node', 'destination_node_id', 'id');
    }

    public function nodes()
    {
        return $this->belongsToMany('App\Node', 'decision_node', 'decision_id', 'node_id');
    }

    public function mechanics()
    {
        return $this->morphToMany('App\Mechanic', 'triggerable');
    }
}
