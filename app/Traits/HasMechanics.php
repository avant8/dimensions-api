<?php

namespace App\Traits;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

trait HasMechanics
{
    public function mechanics()
    {
        return $this->morphToMany('App\Mechanic', 'triggerable');
    }
}
