<?php

namespace App\Mechanic;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $table = 'mechanic_types';
}
