<?php

namespace App\Mechanic;

use Illuminate\Database\Eloquent\Model;

class Triggerable extends Model
{
    public function decisions()
    {
        return $this->morphedByMany('App\Decision', 'triggerable');
    }

    public function nodes()
    {
        return $this->morphedByMany('App\Node', 'triggerable');
    }
}
