<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use Traits\HasMechanics;

    protected $fillable = [
        'name', 'story_id', 'node_type_id', 'description'
    ];

    public function nodes()
    {
        return $this->morphToMany('App\Node', 'object', 'node_objects');
    }
}
