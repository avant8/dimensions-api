<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Story extends Model
{
    protected $fillable = [
        'name', 'variable', 'data',
    ];

    public function nodes()
    {
        return $this->hasMany('App\Node');
    }

    public function games()
    {
        return $this->hasMany('App\Games');
    }
}
