<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\GameVariable;
use Lua;

class Game extends Model
{
    protected $fillable = [
        "story_id",
        "user_id",
    ];

    public function characters()
    {
        return $this->belongsToMany('App\Character');
    }
    public function story()
    {
        return $this->belongsTo('App\Story');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function node()
    {
        return $this->belongsTo('App\Node');
    }

    public function variables()
    {
        return $this->hasMany('App\GameVariable');
    }

    //TODO: Have variables cascade in to game, then save when game is saved?
    public function setVariable($key, $data)
    {
        $variable = GameVariable::where('variable', $key)->where('game_id', $this->id)->first();
        if (!$variable) {
            $variable = GameVariable::create(
                [
                    'game_id' => $this->id,
                    'variable' => $key,
                    'data' => $data
                ]
            );
            return $variable;
        }
        $variable->data = $data;
        $variable->save();
        return $variable;
    }

    /**
     * Take a variables array from LUA and save to DB
     *
     * @param [type] $variables
     * @return void
     */
    public function setVariables($variables)
    {
        foreach ($variables as $key => $value) {
            $this->setVariable($key, $value);
        }
    }

    public function getVariable($key)
    {
        $variable = GameVariable::where('variable', $key)->where('game_id', $this->id)->first();
        if (!$variable) {
            return false;
        }
        return $variable->data;
    }

    /**
     * Prepend story text to node, to be called from a mechanic
     *
     * @param [type] $text
     * @return void
     */
    public static function prependStory($text)
    {
        $node = session("node");
        $node->description = "<p>" . $text . "</p>" . $node->description;
        session(['node' => $node]);
    }

    /**
     * Append story text to node, to be called from a mechanic
     *
     * @param [type] $text
     * @return void
     */
    public static function appendStory($text)
    {
        $node = session("node");
        $node->description .= "<p>" . $text . "</p>";
        session(['node' => $node]);
    }

    /**
     * Runs a mechanic LUA script
     *
     * @param [type] $mechanic
     * @return void
     */
    public function triggerMechanic($mechanic)
    {
        //! IMPORTANT
        //TODO: Sandbox this instead of just eval
        //TODO: Use Laravel Events
        session(['node' => $this->node]);

        $lua = $this->getLua();
        $variables = $mechanic->execute($lua);

        if ($variables)
            $this->setVariables($variables);

        $this->node = session("node");
    }

    public function getLua()
    {
        $lua = new Lua();
        $lua->registerCallback("addMonster", function ($id) {
            Game::addMonster($id);
        });
        $lua->registerCallback("addItem", function ($id) {
            Game::addItem($id);
        });
        $lua->registerCallback("prependStory", function ($text) {
            Game::prependStory($text);
        });
        $lua->registerCallback("appendtory", function ($text) {
            Game::prependStory($text);
        });
        $lua->registerCallback("addDecision", function ($name, $id, $disabled = false) {
            Game::addDecision($name, $id, $disabled);
        });
        $lua->registerCallback("removeDecision", function ($name) {
            Game::addDecision($name);
        });

        foreach ($this->variables as $variable)
            $lua->assign($variable->variable, $variable->data);

        //Make game variables accessible in LUA
        $lua->assign("user", $this->user);
        $lua->assign("game", $this);
        $lua->assign("story", $this->story);
        $lua->assign("node", $this->node);

        //$lua->registerCallback("setVariable", $this->setVariable);
        //$lua->registerCallback("appendStory", "var_dump");

        return $lua;
    }

    public static function removeDecision($name)
    {
        $decisions = session("decisions");
        if (!is_array($decisions))
            $decisions = array();

        $newDec = array();

        foreach ($decisions as $dec) {
            if (!$dec['name'] == $name)
                $newDec = $dec;
        }
        //$decisions[] = array('id' => 0, 'destination_node_id' => $id, 'name' => $name, 'disabled' => $disabled);

        session(['decisions' => $newDec]);
    }

    public static function addDecision($name, $id, $disabled = false)
    {
        $decisions = session("decisions");
        if (!is_array($decisions))
            $decisions = array();


        $decisions[] = array('id' => 0, 'destination_node_id' => $id, 'name' => $name, 'disabled' => $disabled);

        session(['decisions' => $decisions]);
    }

    //TODO: DOn't use sessions
    public static function addItem($id)
    {
        $item = Item::find($id);
        $items = session("items");
        if (!is_array($items))
            $items = array();

        $items[] = $item;

        session(['items' => $items]);
    }

    public static function addMonster($id)
    {
        $monster = Monster::find($id);
        $monsters = session("monsters");
        if (!is_array($monsters))
            $monsters = array();

        $monsters[] = $monster;

        session(['monsters' => $monsters]);
    }
}
