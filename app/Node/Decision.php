<?php

namespace App\Node;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Decision extends Model
{
    protected $table = 'node_decisions';
    public $timestamps = false;

    protected $fillable = [
        'name', 'variable', 'node_id', 'destination_node_id'
    ];

    public function destination_node()
    {
        return $this->belongsTo('App\Node', 'destination_node_id', 'id');
    }

    public function node()
    {
        return $this->belongsTo('App\Node');
    }

    public function mechanics()
    {
        return $this->morphToMany('App\Mechanic', 'triggerable');
    }
}
