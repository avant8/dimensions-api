<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Character extends Model
{
    protected $fillable = [
        'name', 'story_id', 'node_type_id', 'description'
    ];

    public function game()
    {
        return $this->belongsToMany('App\Game', 'game_characters', 'character_id', 'game_id');
    }
}
