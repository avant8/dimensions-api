<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('story', 'StoryController@index');
Route::get('story/upload', 'StoryController@upload');
Route::post('story/import', 'StoryController@import');

Route::get('story/{story}', 'StoryController@show');

Route::post('game', 'GameController@store');

Route::post('game/new', 'GameController@store');

//TODO: Update game decision route (game/{Id}/decision/??)
Route::post('game/{game}/destination/{destination}/decision/{decision}', 'GameController@decision');
Route::get('game/{game}', 'GameController@show');

Route::post('node', 'NodesController@store');
Route::get('node/{node}', 'NodeController@show');
