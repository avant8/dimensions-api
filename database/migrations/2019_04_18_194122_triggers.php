<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Triggers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monsters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->nullable();
            $table->text('name');
            $table->text('description')->nullable();
            $table->text('notes')->nullable();
            $table->text('traits')->nullable();
            $table->text('image')->nullable();
            $table->integer('hp')->nullable();
            $table->integer('str');
            $table->integer('dex');
            $table->integer('int');
            $table->integer('cha');
            $table->timestamps();
        });
        Schema::create('items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->nullable();
            $table->text('name');
            $table->text('description')->nullable();
            $table->text('notes')->nullable();
            $table->text('image')->nullable();
            $table->integer('str')->nullable();
            $table->integer('dex')->nullable();
            $table->integer('int')->nullable();
            $table->integer('cha')->nullable();
            $table->integer('req_str')->nullable();
            $table->integer('req_dex')->nullable();
            $table->integer('req_int')->nullable();
            $table->integer('req_cha')->nullable();
            $table->timestamps();
        });
        Schema::create('monster_node', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('node_id');
            $table->integer('monster_id');
        });
        Schema::create('item_node', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('node_id');
            $table->integer('item_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('triggers');
    }
}
